# Moment

Projet Arts+Sciences autour de la thèse d'[Anaïs Machard](https://www.linkedin.com/in/anaismachard/) : "Contribution à la Conception des Bâtiments sous la Contrainte du Changement Climatique", au Laboratoire [LaSIE](https://lasie.univ-larochelle.fr/) de l'université de La Rochelle.

Avec l'artiste multimédia [Jérôme Abel](http://jeromeabel.net).

## Documents
Historique, processus de création et questionnements sur le [framapad/yybjsuisns](https://semestriel.framapad.org/p/yybjsuisns).

Deux axes se dessinent :
- Installation sur les différents futurs
- Installation focalisée sur une technique de refroidissement des batiments

## Axe #1 - Commitcraty

<img src="dessins/axe1-vue.jpg" alt="Axe 1 : vue générale" width="700px">

Dans une pièce, des dessins sont collés au murs pour former des frises historiques,  des lignes de temps. L'idée est de visualiser les différents futurs, d'aujourd'hui à 2100 par exemple, période de projection des scénarios sur le dérèglement climatique.

Les études montrent deux grandes tendances des scénarios sur le réchauffement climatique, dont la bifurcation se situe entre 2040 et 2070, période choisie pour la thèse. Ces scénarios nous alertent et nous mettent face aux différents choix possibles. Les frises se croisent, émergent, disparaissent, s'unissent pour signifier les batailles de l'imaginaire en dégradé, entre le défaitisme et le positivisme.

Les frises sortent de l'exposition pour continuer dans la ville, sortir du cadre conventionnel et s'exposer vraiment aux habitants.

Exemples : Git Flow, Buren.

<img src="dessins/axe1-plan.jpg" alt="Axe 1 : plan masse" width="700px">

Sur les frises, chaque dessin représente par exemple une année. Le fond du dessin est toujours le même endroit. Pour commencer il s'agit d'un plan masse du batîment choisi pour la thèse, dans lequel vivent des habitants. Les différents futurs seront explorés en faisant vivre tout un tas de chose à ce bâtiment et aux habitants : vie quotidienne, vieillissement, vie joyeuse, canicule, montée des eaux, feux, guerre civile, habitation passive/écologique, invasion de rats, destruction, reconstruction, etc.

Exemple : SIMS.

<img src="dessins/axe1-atelier.jpg" alt="Axe 1 : atelier futur" width="700px">

L'installation demande une quantité de dessins très importantes et nous pourrions organiser des ateliers pour les produire, avec des enfants, adultes, étudiants, graphistes, développeurs. L'idée est de ressentir les différents futurs à travers les styles de dessins qui sont comme l'écriture, uniques pour chacun. Le futur n'est pas une abstraction technologique mais bien incarné par la diversité humaine. 

Nous pourrions associer les écoles d'Angoulême comme partenariat et monter un projet de restitition animée de ce projet en _stop motion_. L'idée d'un programme informatique qui génère les futurs est aussi une possibilité pour réprendre l'utilisation des ordinateurs aujourd'hui dans les phénomènes complexes d'analyse du climat et de prise de décision.

Se dessine peut-être une forme de _commitcraty_ pour reprendre un terme lié à la collaboration dans un projet informatique. Les participants contribuent en proposant un dessin, une forme sur un serveur (_commit_) et l'assemblage de toute cette matière serait discuté pour aboutir à un algorithme de générations de ces futurs.

Exemple : Do.doc (atelier des chercheurs).

## Axe #2 - Fenêtre-cosmos
La forme de cette installation n'est pas arrêtée. Elle pourrait combiner ces différents éléments.

<img src="dessins/axe2-fenetre.jpg" alt="Axe 2 : fenêtre" width="700px">

Assis dans une chaise longue, baigné dans une ambiance calme (pénombre), les spectateurs regardent à travers cette fenêtre les phénomènes de radiation des échanges de chaleur entre la terre et le froid de l'espace.

Solution passive pour lutter contre le réchauffement dans les batiments.


<img src="dessins/axe2-soleil.jpg" alt="Axe 2 : soleil" width="700px">

Jouer sur les ombres et lumières. Une lumières énergi-vores, un soleil très cher qui illumine des blocs transparents composés d'agrégats condensés de matières : essence, fumées, plastique, végétaux, animaux, etc.

<img src="dessins/axe2-stonehenge.jpg" alt="Axe 2 : stonehenge" width="700px">

Même idée avec une disposition rappelant les structures circulaires concentriques du site de Stonehenge.

Exemple : Stonehenge

<img src="dessins/axe2-soleil-fragile.jpg" alt="Axe 2 : soleil fragile" width="700px">

Des fils forment un rideau, un espace de projection d'un cycle jour/nuit. Une lame  descend chaque jour pour enlever les fils petit à petit .

Phénomènes atmosphériques, puissances, grandeurs et fragilité de l'homme.

<img src="dessins/axe2-cycles.jpg" alt="Axe 2 : cycles" width="700px">

Cycle jour/nuit. Economie de moyen. Deux faces.

Exemple : Julius Pop.


## Autres idées
- Microscope / mini film futurs
- Porte automatique
- ...

